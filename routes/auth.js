const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
require('../db/connect')
const User = require('../db/models/User')

router.post('/sign-up', async (req, res) => {
    const { user, email, password } = req.body

    const saltRounds = 10

    bcrypt.hash(password, saltRounds, async (err, hash) => {
        if (err) {
            return console.error(err)
        }
        const userDoc = new User({ _id: "inodd", user, email, hash, servers: [] })

        try {
            const returnDoc = await userDoc.save()

            if (returnDoc) {
                jwt.sign({ user }, 'inoshy', (err, token) => {
                    if (err) {
                        return res.status(401).json({ err })
                    }
                    res.status(200).json({ token, user })
                })
            } else {
                return res.status(401).json({ err: 'Could not save user' })
            }
        } catch (err) {
            res.status(401).json({ err })
        }
    })
})

router.post('/sign-in', (req, res) => {
    const { email, password } = req.body

    User.findOne({ email }, 'hash', (err, user) => {
        if (err) {
            return res.status(401).json({ err })
        }

        bcrypt.compare(password, user.hash, (err, result) => {
            if (err) {
                return res.status(401).json({ err })
            }

            if (result) {
                jwt.sign({ user }, 'inoshy', (err, token) => {
                    if (err) {
                        console.log(err)
                        return res.status(401).json({ err })
                    }
                    res.status(200).json({ token, user })
                })
            } else {
                res.status(401).json({ err: 'Incorrect password' })
            }
        })
    })
})

router.post('/verify', (req, res) => {
    const { token } = req.body

    jwt.verify(token, 'inoshy', (err, decoded) => {
        if (err) {
            console.error(err)
            return res.status(401).json({ err })
        }
        res.sendStatus(200)
    })
})

module.exports = router