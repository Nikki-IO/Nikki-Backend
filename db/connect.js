const { url } = require('./config')
const mongoose = require('mongoose')

main().catch(err => console.log(err))

async function main() {
    await mongoose.connect(url)
}