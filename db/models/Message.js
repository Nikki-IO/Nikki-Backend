const mongoose = require('mongoose')

const MessageSchema = new mongoose.Schema({
    _id: String,
    userID: String,
    channelID: String,
    serverID: String,
    content: String
})

const Message = mongoose.model('', MessageSchema)

module.exports = { Message }