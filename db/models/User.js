const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    _id: String,
    user: String,
    email: String,
    hash: String,
    servers: [String]
})

const User = mongoose.model('users', UserSchema)

module.exports = User