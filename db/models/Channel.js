const mongoose = require('mongoose')

const ChannelSchema = new mongoose.Schema({
    _id: String,
    name: String,
    rolePermsSwitches: {
        type: Map,
        of: String
    },
    userPermsSwitches: {
        type: Map,
        of: String
    }
})

ChannelSchema.methods.setRolePermsSwitches = (roleID) => {
    this.rolePermsOverrides[roleID] = {
        VIEW__CHANNEL__SWITCH: 'default',
        READ__MESSAGE__HISTORY__SWITCH: 'default',
        SEND__MESSAGES__SWITCH: 'default',
        MANAGE__CHANNEL__SWITCH: 'default',
        MANAGE__PERMISSIONS__SWITCH: 'default'
    }
}

ChannelSchema.methods.setUserPermsSwitches = (userID) => {
    this.rolePermsOverrides[userID] = {
        VIEW__CHANNEL__SWITCH: 'default',
        READ__MESSAGE__HISTORY__SWITCH: 'default',
        SEND__MESSAGES__SWITCH: 'default',
        MANAGE__CHANNEL__SWITCH: 'default',
        MANAGE__PERMISSIONS__SWITCH: 'default'
    }
}

const Channel = mongoose.model('', ChannelSchema)

module.exports = { ChannelSchema, Channel }