const mongoose = require('mongoose')
const { ChannelSchema } = require('./Channel')
const { RoleSchema } = require('./Role')
const { MemberSchema } = require('./Member')

const ServerSchema = new mongoose.Schema({
    _id: String,
    name: String,
    ownerID: String,
    members: [MemberSchema],
    roles: [RoleSchema],
    channels: [ChannelSchema],
    invites: [String]
}, { autoCreate: true })

const Server = mongoose.model('servers', ServerSchema)

module.exports = { ServerSchema, Server }