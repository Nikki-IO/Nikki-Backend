const mongoose = require('mongoose')

const RoleSchema = new mongoose.Schema({
    _id: String,
    name: String,
    perms: {
        type: Map,
        of: Boolean
    }
})

RoleSchema.methods.setDefaultOwnerPerms = () => {
    this.perms['VIEW__CHANNELS'] = true
    this.perms['READ__MESSAGE__HISTORY'] = true
    this.perms['SEND__MESSAGES'] = true
    this.perms['CREATE__INVITE'] = true
    this.perms['MANAGE__ROLES'] = true
    this.perms['MANAGE__CHANNELS'] = true
}

RoleSchema.methods.setDefaultMemberPerms = () => {
    this.perms['VIEW__CHANNELS'] = true
    this.perms['READ__MESSAGE__HISTORY'] = true
    this.perms['SEND__MESSAGES'] = true
    this.perms['CREATE__INVITE'] = false
    this.perms['MANAGE__ROLES'] = false
    this.perms['MANAGE__CHANNELS'] = false
}

const Role = mongoose.model('', RoleSchema)

module.exports = { RoleSchema, Role }