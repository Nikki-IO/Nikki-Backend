const mongoose = require('mongoose')

const MemberSchema = new mongoose.Schema({
    _id: String,
    roles: [String]
})

const Member = mongoose.model('', MemberSchema)

module.exports = { MemberSchema, Member }