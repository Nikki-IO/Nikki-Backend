const express = require('express')
const cookie_parser = require('cookie-parser')
const cors = require('cors')
const auth = require('./routes/auth')
const app = express()

const port = 3001

// Decode url encoded data
app.use(
  express.urlencoded({
    extended: false
  })
)

// Parses requst body as json
app.use(express.json())

// Set and read cookies
app.use(cookie_parser())

app.use(cors({ origin: 'http://localhost:3000' }))

app.use('/auth', auth)

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
